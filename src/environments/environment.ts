// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDT7CSmSZRyTXqTylKDxvOEwbvJVqAVN2E',
    authDomain: 'test-task-f1536.firebaseapp.com',
    databaseURL: 'https://test-task-f1536.firebaseio.com',
    projectId: 'test-task-f1536',
    storageBucket: '',
    messagingSenderId: '716135110731'
  }
  // firebase: {
  //   apiKey: 'AIzaSyC0EduRswAkIbcH33FgvbwwLMWZOhLDj6g',
  //   authDomain: 'student-cedd5.firebaseapp.com',
  //   databaseURL: 'https://student-cedd5.firebaseio.com',
  //   projectId: 'student-cedd5',
  //   storageBucket: '',
  //   messagingSenderId: '562284980249'
  // }
};
