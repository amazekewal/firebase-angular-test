import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './pages/login/signin.component';
import { SignupComponent } from './pages/signup/signup.component';
import { StudentComponent } from './pages/student/student.component';
import { MentorStudentsComponent } from './pages/mentor-students/mentor-students.component';
import { AuthGuardService } from './services/auth-guard.service';
import { StudentGuardService } from './services/student-guard.service';
import { MentorGuardService } from './services/mentor-guard.service';

import { HomeComponent } from './pages/home/home.component';

const appRoutes: Routes = [
  {
    path: '', redirectTo: 'home', pathMatch: 'full'
  },
  { path: 'home', component: HomeComponent },
  { path: 'sign-in', component: SigninComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'student', component: StudentComponent, canActivate: [AuthGuardService, StudentGuardService] },
  { path: 'mentor', component: MentorStudentsComponent, canActivate: [AuthGuardService, MentorGuardService] },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
