import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';



@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.sass']
})
export class SigninComponent implements OnInit {

  @ViewChild(NgForm) form;

  user = {
    photoURL: null,
    displayName: null,
    email: null,
    password: null,
    type: null,
    type_center: null,
    points: null
  };

  constructor(
    private auth: AuthService,
    private router: Router
  ) {
    this.auth.getAuthState().subscribe((user) => {
      if (user !== null) {
        this.router.navigate(['/home']);
      }
    });
  }

  ngOnInit() {

  }
  signIn() {
    if (this.form.valid) {
      this.auth.signIn(this.user)
        .then(data => {
          this.router.navigate(['/home']);
        })
        .catch(err => {
          console.log('Something went wrong:', err.message);
        });
    }
  }
}
