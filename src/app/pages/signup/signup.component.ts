import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.sass']
})
export class SignupComponent implements OnInit {
  @ViewChild(NgForm) form;

  user = {
    photoURL: null,
    displayName: null,
    email: null,
    password: null,
    type: null,
    type_center: null,
    points: null
  };

  constructor(
    public auth: AuthService,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.auth.getAuthState().subscribe((user) => {
      if (user !== null) {
        this.router.navigate(['/home']);
      }
    });
  }

  signUp() {
    if (this.form.valid) {
      this.auth.signUp(this.user)
        .then(data => {
          // console.log(data);
          this.auth.updateUserProfile(data, this.user)
            .then(res => {
              console.log('why??', res);
              this.router.navigate(['/home']);
            })
            .catch(err => console.log(err.message));
        })
        .catch(err => {
          console.log('Something went wrong:', err.message);
        });
    }
  }

}
