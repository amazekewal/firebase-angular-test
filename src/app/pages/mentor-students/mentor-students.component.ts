import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-mentor-students',
  templateUrl: './mentor-students.component.html',
  styleUrls: ['./mentor-students.component.sass']
})
export class MentorStudentsComponent implements OnInit {

  constructor(
    public auth: AuthService
  ) {
    this.auth.getAllUsers();
  }

  ngOnInit() {
  }

}
