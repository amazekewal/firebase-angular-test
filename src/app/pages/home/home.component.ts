import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  constructor(
    private auth: AuthService,
    private router: Router
  ) {
    this.auth.getAuthState().subscribe(data => {
      console.log(data);
      if (data && data.displayName) {
        if (JSON.parse(data.displayName).type === 'student') {
          this.router.navigate(['/student']);
          this.router.navigate(['/student']);
        }
        if (JSON.parse(data.displayName).type === 'mentor') {
          this.router.navigate(['/mentor']);
          this.router.navigate(['/mentor']);
        }
      }
    });
  }

  ngOnInit() {
  }

}
