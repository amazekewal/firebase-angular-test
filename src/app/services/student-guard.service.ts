import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from './auth.service';

@Injectable()
export class StudentGuardService {
  constructor(
    private auth: AuthService,
    private router: Router
  ) {
  }
  canActivate() {
    if (JSON.parse(this.auth.authState.displayName).type === 'student') {
      return true;
    } else {
      this.router.navigate(['/student']);
      return false;
    }
  }
}
