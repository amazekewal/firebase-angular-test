import { Injectable } from '@angular/core';
import { CanActivate, CanDeactivate } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AuthGuardService {
  authState: any = null;
  constructor(
    public afAuth: AngularFireAuth,
  ) {
    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth;
    });
  }

  canActivate() {
    return this.authState !== null;
  }
}
