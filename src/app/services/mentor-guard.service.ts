import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from './auth.service';

@Injectable()
export class MentorGuardService {
  constructor(
    private auth: AuthService,
    private router: Router
  ) {
  }
  canActivate() {
    if (JSON.parse(this.auth.authState.displayName).type === 'mentor') {
      return true;
    } else {
      this.router.navigate(['/mentor']);
      return false;
    }
  }
}
