import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class AuthService {
  authState: any = null;
  constructor(
    public afAuth: AngularFireAuth,
    public db: AngularFireDatabase,
  ) {
    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth;
      console.log(this.authState);
    });
  }

  getAuthState() {
    return this.afAuth.authState;
  }

  isLoggedIn() {
    return this.authState !== null;
  }



  loginWithGoogle() {
    return this.afAuth.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider());
  }
  loginWithFacebook() {
    return this.afAuth.auth.signInWithPopup(
      new firebase.auth.FacebookAuthProvider());
  }
  signUp(user) {
    return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
  }
  updateUserProfile(userObject, user) {
    // console.log(userObject);
    return userObject.updateProfile({
      photoURL: user.photoURL,
      displayName: JSON.stringify({
        displayName: user.displayName,
        type: user.type,
        type_center: user.type_center,
        points: user.points
      }),
    });
  }
  signIn(user) {
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
  }

  getAllUsers() {
    this.db.list('/users').valueChanges().subscribe(data => {
      console.log(data);
    });
  }

  logout() {
    console.log(this.authState);
    return this.afAuth.auth.signOut();
  }
}
