import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router, NavigationStart } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  isRole;
  constructor(
    public auth: AuthService,
    public router: Router
  ) {
    this.getRole();
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.getRole();
      }
    });
  }

  ngOnInit() {
  }

  isLoggedIn() {
    return this.auth.isLoggedIn();
  }

  getRole() {
    this.auth.getAuthState().subscribe(user => {
      if (user && user.displayName) {
        this.isRole = JSON.parse(user.displayName).type;
      }
    });
  }

  loggedOut() {
    this.auth.logout()
      .then(data => {
        console.log(data);
        this.router.navigate(['/sign-in']);
      })
      .catch(err => {
        console.log('Something went wrong:', err.message);
      });
  }

}
